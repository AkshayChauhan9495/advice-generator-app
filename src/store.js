import CombinedReducers from "./combinedReducer/combinedReducer";
import { createStore } from "redux";

const store = createStore(CombinedReducers);

export default store;
