import React, { Component } from "react";
import dice from "./images/icon-dice.svg";
import divider from "./images/pattern-divider-desktop.svg";
import "./AdviceGenerator.css";
import Loader from "./Loader";
import getData from "./../actions/index";
import { connect } from "react-redux";

const mapStateProps = (props) => {
  return {
    item: props.adviceItem,
  };
};

const mapDispatch = (dispatch) => {
  return {
    getData: (value) => dispatch(getData(value)),
  };
};

class AdviceGenerator extends Component {
  fetchData = () => {
    fetch("https://api.adviceslip.com/advice")
      .then((response) => response.json())
      .then((data) => {
        // console.log(data.slip.id);
        // this.setState({
        //   id: data.slip.id,
        //   advice: data.slip.advice,
        //   fetchingData: true,
        // });
        // console.log(data);
        this.props.getData({
          id: data.slip.id,
          advice: data.slip.advice,
          fetchingData: true,
        });
      });
  };
  componentDidMount() {
    this.fetchData();
  }

  handleClick = () => {
    this.props.getData({
      fetchingData: false,
    });
    this.fetchData();
  };
  render() {
    console.log(this.props.item);
    // console.log(this.state);
    return (
      <div>
        {this.props.item.fetchingData ? (
          <div className="d-flex align-items-center justify-content-center body-color">
            <div className=" cardAdvice">
              <div className="card-body d-flex flex-column justify-content-center align-items-center inner-card p-5">
                <div className="card-title font-color">
                  <> ADVICE # </>
                  {this.props.item.id}
                </div>
                <p className="card-text advice">
                  <q>{this.props.item.advice}</q>
                </p>
                <div className="divider">
                  <img src={divider} className="w-100" />
                </div>
                <div className="btn-dice" onClick={this.handleClick}>
                  <div>
                    <img src={dice} className="dice-icon" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <Loader />
        )}
      </div>
    );
  }
}

export default connect(mapStateProps, mapDispatch)(AdviceGenerator);
