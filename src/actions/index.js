const getData = (data) => {
  return {
    type: "getAdvice",
    payload: data,
  };
};

export default getData;
