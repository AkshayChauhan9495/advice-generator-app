import React, { Component } from "react";
import AdviceGenerator from "./components/AdviceGenerator";

export default class App extends Component {
  render() {
    return (
      <div>
        <AdviceGenerator />
      </div>
    );
  }
}
