const reducedData = (state = "", action) => {
  switch (action.type) {
    case "getAdvice": {
      state = action.payload;
      return state;
    }

    default:
      return state;
  }
};

export default reducedData;
