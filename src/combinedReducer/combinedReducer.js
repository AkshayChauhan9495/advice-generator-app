import reducedData from "../reducers/reducer";
import { combineReducers } from "redux";

const CombinedReducers = combineReducers({
  adviceItem: reducedData,
});

export default CombinedReducers;
